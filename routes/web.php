<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('master');
})->name('start');

Route::get('/users/{user_id}', 'UserController@show')->name('userpage');

Route::get('/places/{place_id}', 'PlaceController@show')->name('placepage');

Route::post('/storeuser', 'UserController@store')->name('storeuser');

Route::post('/storereview', 'ReviewController@store')->name('review.store');

Route::post('/updatereview', 'ReviewController@update')->name('review.update');

Route::get('/deletereview/{id}', 'ReviewController@destroy')->name('review.delete');

Auth::routes([
  'verify' => false,
  'reset' => false
]);

Route::get('/home', 'HomeController@index')->name('home');
