<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Sleepy',
            'email' => Str::random(10).'@gmail.com',
            'password' => bcrypt('password'),
            'created_at' => date("Y-m-d H:i:s"),
        ]);

        DB::table('users')->insert([
            'name' => 'Grumpy',
            'email' => Str::random(10).'@gmail.com',
            'password' => bcrypt('password'),
            'created_at' => date("Y-m-d H:i:s"),
        ]);

        DB::table('users')->insert([
            'name' => 'Happy',
            'email' => Str::random(10).'@gmail.com',
            'password' => bcrypt('password'),
            'created_at' => date("Y-m-d H:i:s"),
        ]);

        DB::table('users')->insert([
            'name' => 'Bashful',
            'email' => Str::random(10).'@gmail.com',
            'password' => bcrypt('password'),
            'created_at' => date("Y-m-d H:i:s"),
        ]);

        DB::table('users')->insert([
            'name' => 'Sneezy',
            'email' => Str::random(10).'@gmail.com',
            'password' => bcrypt('password'),
            'created_at' => date("Y-m-d H:i:s"),
        ]);

        DB::table('users')->insert([
            'name' => 'Dopey',
            'email' => Str::random(10).'@gmail.com',
            'password' => bcrypt('password'),
            'created_at' => date("Y-m-d H:i:s"),
        ]);

        DB::table('users')->insert([
            'name' => 'Doc',
            'email' => Str::random(10).'@gmail.com',
            'password' => bcrypt('password'),
            'created_at' => date("Y-m-d H:i:s"),
        ]);
    }
}
