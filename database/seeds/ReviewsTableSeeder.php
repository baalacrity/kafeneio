<?php

use Illuminate\Database\Seeder;

class ReviewsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cafes = ['ChIJ87L0_pLncUgR3iW9ioOCPbw', 'ChIJuY3pMsbmcUgRcwOJCeH4yWI', 'ChIJjQ2cd8jmcUgR_2pQDOCDl_c', 'ChIJgwb15cfmcUgRykOveBJ_GUc', 'ChIJdVC3zLjmcUgRPsjv3i5JUe0', 'ChIJXYog587mcUgRl0yor5-wTVY', 'ChIJY54GV-vmcUgRt2x-zBXzf0Y', 'ChIJ0-9rhV_ncUgRJY7f4kqQYwQ', 'ChIJ7xuO1cLmcUgReNy27pBBQJE', 'ChIJr0tcocbmcUgRHW37O4Ql-RY', 'ChIJJQ0ZBunmcUgREk2RvWRdz5Y', 'ChIJfzVJjczncUgRLDZPXvRZTrw', 'ChIJbfXNm7XmcUgRoUrCGH4vaow', 'ChIJM5IG-ufmcUgRef7ukRNddjs', 'ChIJtUl9N8_mcUgRPHaYY9XTPYc', 'ChIJJStUB7nmcUgR5XcjLXEaSHk', 'ChIJBfV-us7mcUgRn3fXO0hfLzA', 'ChIJgdfhHM_mcUgRDrFdpn1GV5w', 'ChIJrV8n9hzncUgRja0DYg2AzmU', 'ChIJe3xmJaXmcUgReFbl7SUAhKA'];

        $tearooms = ['ChIJCxZqUHLhcUgRpHfnYiHq7Yw', 'ChIJf87pa8rncUgR6d9TdIhmCMw', 'ChIJD959STLncUgRRi-HYteatrs', 'ChIJjSnaC4rlcUgRRlPhI06OH7I', 'ChIJD8l79lXncUgRctkS0vLcYz8', 'ChIJ05vAuc7mcUgRXxPWe4m51jE', 'ChIJgVz37A3ecUgRaALh4X6wxdw', 'ChIJMSf2Q3LncUgRPLMlWLKqkLU', 'ChIJkbgnW7AcbkgRP5G2ZXdlnpM', 'ChIJ0TxZlYTecUgR3bbGpd_wOK0', 'ChIJPQTnVEYZbkgRKHdGzIBmx9w', 'ChIJiXzM8XEcbkgRqwsdZQA3p-E', 'ChIJaeNhn68cbkgRchlMXi7Rjkg', 'ChIJSXvKnuccbkgR--I3uR1WtBo', 'ChIJQe-qxF7wcUgRiHeM4yrZKqc', 'ChIJm1dt52obbkgR1yKM2IHoHzw', 'ChIJNeb3WrEcbkgRGOyAPfOLDvQ', 'ChIJZbbngAPxcUgRfwq2L-GcN6o', 'ChIJUZAySXoDbkgRB-0YxPQBRJ4', 'ChIJ13MHkEH5cUgRLFDQjcX71j4'];

        $bars = ['ChIJU4p-HsbmcUgR5DaRNyfK948', 'ChIJHxaUo8bmcUgRFuH8VaWqJxU', 'ChIJQcxTYcfmcUgRCyWTeG0q-sE', 'ChIJA9LQTs_mcUgRtqWxDNhmohE', 'ChIJ256FHs_mcUgRTEHbgGHYF0o', 'ChIJbfXNm7XmcUgRoUrCGH4vaow', 'ChIJFUk4c8XmcUgRGlxJ_Q6_Zg4', 'ChIJERkvEM_mcUgRpgLqYMKKwns', 'ChIJXQGB787mcUgRSB7iSzfJetI', 'ChIJ6zoa4s7mcUgR6G1HX-KjEks', 'ChIJ-3NmE8XmcUgRiK75k0qvVSo', 'ChIJN14K9sfmcUgRugGqFEREytE', 'ChIJh7Gp4c7mcUgRPA2vVc8-eEg', 'ChIJdd4Z4cTmcUgRWrdEGgyTz1A', 'ChIJUyFNc7jmcUgR-geVp3kDZ3A', 'ChIJk5Sw38fmcUgR0ItC-Z_WAI0', 'ChIJVaxj4MfmcUgRdoMMkm4011g', 'ChIJ8zoW487mcUgRcrbcbrbUlc4', 'ChIJm4Lc3sfmcUgRjQSzFAvsLsY', 'ChIJIy_T_M7mcUgR0oOiCPKU5Y4'];

        for ($j = 0; $j < 7; $j++)
        {
	        for ($i = 0; $i < count($cafes); $i++)
	        {
		        DB::table('reviews')->insert([
		            'place_id' => $cafes[$i],
		            'user_id' => $j + 1,
		            'rating' => rand(1, 10),
		            'title' => Str::random(10),
		            'comment' => Str::random(100),
		            'created_at' => date("Y-m-d H:i:s"),
		        ]);
		        DB::table('reviews')->insert([
		            'place_id' => $tearooms[$i],
		            'user_id' => $j + 1,
		            'rating' => rand(1, 10),
		            'title' => Str::random(10),
		            'comment' => Str::random(100),
		            'created_at' => date("Y-m-d H:i:s"),
		        ]);
		        DB::table('reviews')->insert([
		            'place_id' => $bars[$i],
		            'user_id' => $j + 1,
		            'rating' => rand(1, 10),
		            'title' => Str::random(10),
		            'comment' => Str::random(100),
		            'created_at' => date("Y-m-d H:i:s"),
		        ]);
	        }
	    }
    }
}
