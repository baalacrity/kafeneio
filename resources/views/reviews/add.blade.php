<form class="form-group" action="{{ route('review.store') }}" method="POST">
    <h4>Leave a review:</h4>
	    <input type="text" class="form-control" name="title" placeholder="Title"><br>
	    <textarea type="text" class="form-control" name="comment" placeholder="Comment" rows=3></textarea>
	    <div class="form-group">
	    Rating: <input class="form-control" type="number" name="rating" min="1" max="10" step="0.5" placeholder="10">
		</div>

    <input type="hidden" name="place_id" value="{{ $place_id }}">
    <input type="hidden" name="user_id" value="{{ Auth::id() }}">
    <input class="btn btn-primary" type="submit" value="Submit">
    @csrf
</form>