<?php
	$tempReview = new App\Review;
	$tempReview->user_id = $review->user_id;

	$reviewerName = $tempReview->user->name;
?>

<div class="card">
	<div class="card-body">
		<form id="review-form-{{ $review->id }}" action="{{ route('review.update') }}" method='POST'>
		<div id="review-{{ $review->id }}">
		<h4 id="review-title-{{ $review->id }}" class="card-title">{{ $review->title }} ・ <span style="color:gray">{{ $review->rating }}/10</span></h4>
		<p id="review-comment-{{ $review->id }}" class='card-text'>"{{ $review->comment }}"</p>
		<small class="card-subtitle mb-2 text-muted"><i> <span style="color:gray;font-weight:lighter">By </span>{{ $reviewerName }} <span style="color:gray;font-weight:lighter"> at {{ $review->created_at }}</span></i></small>
		</div>
		@csrf
	</form>
		<div id="editDeleteButtons" display="">
		@if (Auth::id() == $review->user_id)
		<button id="review-edit-{{ $review->id }}" onclick="editReview('{{ $review->id }}', '{{ $review->title }}', '{{ $review->comment }}', '{{ $review->rating }}')" type="button" class="btn btn-outline-success">Edit</button>
		<a id="review-delete-{{ $review->id }}" href="{{ route('review.delete', ['id' => $review->id]) }}" class="btn btn-outline-danger">Delete</a>
		@endif
		</div>
		<div id="saveCancelButtons" style="display: none">
		<button id="review-save-{{ $review->id }}" form="review-form-{{ $review->id }}" type="button submit" class="btn btn-outline-success">Save</button>
		<button id="review-cancel-{{ $review->id }}" onclick="cancelEditReview('{{ $review->id }}', '{{ $review->title }}', '{{ $review->comment }}', '{{ $review->rating }}')" type="button" class="btn btn-outline-danger">Cancel</button>

		</div>
	</div>
</div>
<script type="text/javascript" src="/js/editreview.js"></script>