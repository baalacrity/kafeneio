<html>
	<head>
        <meta name='viewport' content='initial-scale=1.0, user-scalable=no'>
        <meta charset='utf-8'>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <title id="titleName">Loading...</title>
        <script>
        	function getPlaceDist(placeLocation)
			{
			    navigator.geolocation.getCurrentPosition(function(location){
			        var currentLoc = new google.maps.LatLng(location.coords.latitude, location.coords.longitude);
					var distService = new google.maps.DistanceMatrixService();

				    distService.getDistanceMatrix(
				      {
				        origins: [currentLoc],
				        destinations: [placeLocation],
				        unitSystem: google.maps.UnitSystem.IMPERIAL,
				        travelMode: "WALKING"
				      }, function distCallback(response, status)
				    {
				        document.getElementById('placeDistance').innerHTML = response.rows[0].elements[0].distance.text;
				    });
				});
			}


        	function getPlace(){
	        	var currPlace;
	        	var currPlaceId = "{{ $place_id }}";
	        	var service = new google.maps.places.PlacesService(document.createElement('div'));

	        	service.getDetails({ placeId: currPlaceId }, function(place){
	        		document.getElementById('titleName').innerHTML = "Kafeneio: " + place.name;
	        		document.getElementById('placeName').innerHTML = place.name;
	        		document.getElementById('placeAddress').innerHTML = place.formatted_address;
	        		document.getElementById('placePhoto').setAttribute("src", place.photos ? place.photos[0].getUrl() : "https://cdn.writermag.com/2019/03/question-marks.jpg");
	        		getPlaceDist(place.geometry.location);

	        		var openData = place.opening_hours;
	        		if (openData)
	        		{
	        			/*var d = new Date();
	        			var dayOfWeek = d.getDay();

	        			var nextOpeningString, nextClosingString;
	        			if (openData.isOpen())
	        			{
	        				if (openData.weekday_text.close.day != dayOfWeek)
	        					nextClosingString = "; open all day";
	        				else
	        					nextClosingString = "; closes at " + openData.periods[dayOfWeek].close.hours + ":" + (openData.periods[dayOfWeek].close.minutes ? openData.periods[dayOfWeek].close.minutes : '00');

	        				document.getElementById('placeOpenOrNot').innerHTML = '<span style="color: blue">Open</span>' + nextClosingString + ".";
 	        			}
 	        			else
	        			{
	        				var allDaysOfWeek = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
	        				if (openData.periods[dayOfWeek].open.day != dayOfWeek)
	        					nextOpeningString = "; opens on " + allDaysOfWeek[openData.periods[dayOfWeek].open.day] + " at ";
	        				else
	        					nextOpeningString = "; opens at ";

	        				nextOpeningString += openData.periods[dayOfWeek].open.hours + ":" + (openData.periods[dayOfWeek].open.minutes ? openData.periods[dayOfWeek].open.minutes : '00');
*/
	        				document.getElementById('placeOpenOrNot').innerHTML = (openData.isOpen() ? '<span style="color: blue">Open</span>' : '<span style="color: red">Closed</span>');/*
	        				document.getElementById('placeWeekDay-1').innerHTML = openData.weekday_text[0];
	        				document.getElementById('placeWeekDay-2').innerHTML = openData.weekday_text[1];
	        				document.getElementById('placeWeekDay-3').innerHTML = openData.weekday_text[2];
	        				document.getElementById('placeWeekDay-4').innerHTML = openData.weekday_text[3];
	        				document.getElementById('placeWeekDay-5').innerHTML = openData.weekday_text[4];
	        				document.getElementById('placeWeekDay-6').innerHTML = openData.weekday_text[5];
	        				document.getElementById('placeWeekDay-7').innerHTML = openData.weekday_text[6];*/
 	        			//}
	        		}
	        		else
	        			document.getElementById('placeOpenOrNot').innerHTML = '<i>No information</i>';


	        		document.getElementById('placeUrl').setAttribute("href", place.url);
	        	});
	        }
        </script>
        <script src='https://maps.googleapis.com/maps/api/js?key=AIzaSyD2F-40-UKwBOyyaQ6hZRF6yFHCApu9gM4&libraries=places&callback=getPlace' async defer></script>
        <style>
        	#placeName {
        		font-size: 30px;
        	}

        	.container.card {
        		margin: 20px;
        		padding: 20px;
        	}
        	.container.card .row {
        		height: 400px;
        	}
        	.grey_bg {
        		background-color: gray;
        	}
        	.card {
        		text-align:left;
        	}
	          .page-header {
	            padding: 10px;
	            margin: 10px;
	          }
	          .collapse.card {
	          	padding: 20px;
	          }
	          .table.table-borderless {
	          	text-align: left;
	          }
	          .card-img-top {
				  object-fit: cover;
				  height: 100%;
	          }
	          .card-subtitle {
	          	font-size: 16pt;
	          }
           body {
            background: url("/../main_bg2.jpg");
            background-size: 100%;
            background-repeat: no-repeat; /* Do not repeat the image */
            background-attachment: fixed;

          }
        </style>
	</head>
	<body>
        <div class='page-header' align='center'>
              <a href="/"><img src="/../banner_gfx2.png" class="img-fluid"></a>
        </div>

        @include('/../navbar')

        <div align="center">
			<div class="container card">
				<div class="row">
					<div class="card-body col-6">
						<h2 id="placeName" class="card-title"></h2></tr>
						<table class="table table-borderless">
							<tbody>
								<tr>
									<td colspan=2><a id="placeUrl" href="#">Open on Google Maps</a></td>
								</tr>
								<tr>
									<th>Address:</th>
									<td id="placeAddress"></td>
								</tr>
								<tr>
									<th>Distance:</th>
									<td id="placeDistance"></td>
								</tr>
								<tr>
									<th>Hours:</th>
									<td id="placeOpenOrNot"></td>
								</tr>
								<tr>
									<th colspan=2><br><h6 class="card-subtitle mb-2">Kafeneio rating:</h6></th>
								</tr>
								<tr>
									<th colspan=2><h2><?= App\Review::getMeanRating($place_id) ?></h2></th>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="col-6">
						<img id="placePhoto" class="card-img-top" src="#">
					</div>
				</div>
			</div>
			<div class="container card">
				<h2>Reviews:</h2>
				<?php 
					$alreadyHasReview = false;
					if (is_countable($reviews) == false || count($reviews) == 0)
					{
						echo "<br><h6 class='card-subtitle'>No reviews.</h6>";
					}
					else if (App\Review::checkIfAlreadyHasReview(Auth::id()))
						$alreadyHasReview = true;
				?>
				@if (Auth::check())
					<br><a class="btn btn-primary" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">Add Review</a>
							<div class="collapse card" id="collapseExample">
								@include('reviews/add')
							</div>
				@else
					<br><a class="btn btn-primary" href="{{ route('login') }}" role="button">You must log in to leave a review.</a>
				@endif
				@foreach($reviews as $review)
					@include('reviews/template')
				@endforeach
			</div>
		</div>

    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
	</body>
</html>