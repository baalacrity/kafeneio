<html>  
    <head>
        <meta name='viewport' content='initial-scale=1.0, user-scalable=no'>
        <meta charset='utf-8'>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link href="/css/homepage.css" rel="stylesheet">
        <script type="text/javascript" src="/js/gmaps.js"></script>
        <title>Kafeneio</title>
    </head>
    <body>
        <div class='page-header' align='center'>
              <img src="banner_gfx2.png" class="img-fluid">
        </div>

        <div class="container tab">
            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <div class="collapse navbar-collapse" id="navbarNav">
                    <ul class="navbar-nav col-8">
                        <li class="nav-item col-2 active">
                          <a id="cafe" class="nav-link" href="javascript:openPage('cafe')">Coffee</a>
                        </li>
                        <li class="nav-item col-2">
                          <a ig="tea room" class="nav-link" href="javascript:openPage('tea room')">Tea</a>
                        </li>
                        <li class="nav-item col-2">
                          <a id="bar" class="nav-link" href="javascript:openPage('bar')">Bars</a>
                        </li>
                    </ul>
                </div>
                @if (Auth::check())
                <div class="col-3" style="text-align: right">
                    Welcome back, <b>{{ Auth::user()->name }}</b>!
                </div>
                |
                <div class="col-1" style="text-align: left">
                    <a class="link" href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                                     document.getElementById('logout-form').submit();">Logout
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </div>
                @else
                <div class="col-1" style="text-align: right">
                    <a href="{{ route('login') }}">Login</a>
                </div>
                |
                <div class="col-1" style="text-align: left">
                    <a href="{{ route('register') }}">Register</a>
                </div>
                @endif
            </nav>
            <div id="placeList"></div>
        </div>  

        <script src='https://maps.googleapis.com/maps/api/js?key=AIzaSyD2F-40-UKwBOyyaQ6hZRF6yFHCApu9gM4&libraries=places&callback=initialise' async></script>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    </body>
    <footer align="center">
       <a href="https://pngtree.com/free-backgrounds">Background courtesy of pngtree.com</a>
    </footer>
</html>