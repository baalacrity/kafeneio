<div class="container">
            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <div class="collapse navbar-collapse" id="navbarNav">
                    <ul class="navbar-nav col-8">
                        <li class="nav-item">
                          <a id="bar" class="nav-link" href="{{ route('start')}}">Home</a>
                        </li>
                    </ul>
                </div>
                @if (Auth::check())
                <div class="col-3" style="text-align: right">
                    Welcome back, <b>{{ Auth::user()->name }}</b>!
                </div>
                |
                <div class="col-1" style="text-align: left">
                    <a class="link" href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                                     document.getElementById('logout-form').submit();">Logout
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </div>
                @else
                <div class="col-1" style="text-align: right">
                    <a href="{{ route('login') }}">Login</a>
                </div>
                |
                <div class="col-1" style="text-align: left">
                    <a href="{{ route('register') }}">Register</a>
                </div>
                @endif
            </nav>
            <div id="placeList"></div>
        </div>
