@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">

	<div class="card container">
		<div class="card-body">
			<div class="card-title"><h2>{{ $user->name }}</h2></div>
			<div class="card-text">
				<i>Joined: {{ $user->created_at }}</i>
			</div>
		</div>
	</div>

	<div class="card container">
		<div class="card-body">
			<h3 style="font-size:16pt">Reviews by {{ $user->name }}:</h3></div>
	</div>

	@foreach ($user->reviews as $review)
		@include('users/profileReview')
	@endforeach


    </div>
</div>
@endsection
