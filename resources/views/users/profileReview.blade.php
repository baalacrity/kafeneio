<div class="card">
  <div class="card-header">
    <h4 class="card-title"><a id="place-name-{{ $review->place_id }}" href="{{ route('placepage', ['place_id' => $review->place_id]) }}">Place Name</a></h4>
  </div>
	<div class="card-body">
		<h4 class="card-title">{{ $review->title }} ・ <span style="color:gray">{{ $review->rating }}/10</span></h4>
		<p class='card-text'>"{{ $review->comment }}"</p>
		<small class="card-subtitle mb-2 text-muted"><i><span style="color:gray;font-weight:lighter">{{ $review->created_at }}</span></i></small>
	</div>
</div>