function openPage(target) {
    // Declare all variables
    var i, tabcontent, navlinks;

    document.getElementById('placeList').innerHTML = "";

    // Get all elements with class="navlinks" and remove the class "active"
    navlinks = document.getElementsByClassName("nav-item");
    for (i = 0; i < navlinks.length; i++) {
        navlinks[i].className = navlinks[i].className.replace(" active", "");
    }

    loadPlaces(target);
}

function initialise()
{
    loadPlaces('cafe');
}

function placeCardGenerator(placeResult, distance)
{
    return "<div class='card'>" +
        "<div class='row'>" +  
        "<div class='col-3'><img class='card-img-top' src='" + (placeResult.photos ? placeResult.photos[0].getUrl() : "https://cdn.writermag.com/2019/03/question-marks.jpg") + "'></div>" + 
        "<div class='card-body col-9'>" + 
        "<h5 class='card-title'>" + placeResult.name + "</h5>" +
        "<p class='card-text'>" + placeResult.formatted_address + "</p>" + 
        "<p class='card-text'><span id='distance-" + placeResult.place_id + "'></span> away</p>" + 
        "<a href='/places/" + placeResult.place_id +  "' class='btn btn-primary'>Reviews</a>" + 
        "</div></div>" + 
    "</div>"
}

function getPlaceDist(currentLoc, destinationLoc, placeId, returnFunc){
    var distService = new google.maps.DistanceMatrixService();
    distService.getDistanceMatrix(
      {
        origins: [currentLoc],
        destinations: [destinationLoc],
        unitSystem: google.maps.UnitSystem.IMPERIAL,
        travelMode: "WALKING"
      }, function distCallback(response, status)
    {
        returnFunc(response.rows[0].elements[0].distance.text, placeId);
    });
}

function loadPlaces(target)
{
    placeResults = [];

    navigator.geolocation.getCurrentPosition(function(location){
        var currentLoc = new google.maps.LatLng(location.coords.latitude, location.coords.longitude);

        var service = new google.maps.places.PlacesService(document.createElement('div'));

        var request = {
            location: currentLoc,
            query: target,
        };

        service.textSearch(request, callbackFunc);
            
        function callbackFunc(results, status) {
            
            var output = "";
            var distances =[];

            for (var i = 0; i < results.length; i++) {

                var distance = getPlaceDist(currentLoc, results[i].geometry.location, results[i].place_id, function(trueValue, placeId){
                    document.getElementById('distance-' + placeId).innerHTML = trueValue;
                });

                output += placeCardGenerator(results[i], distances[i]);
            }
            document.getElementById('placeList').innerHTML = output;
        }
    });
}