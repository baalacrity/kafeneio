var currentContent;

function editReview(id, title, comment, rating)
{

	var editBox = "<input type='text' class='form-control' name='title' value='" + title + "'><br>" + 
	    "<textarea type='text' class='form-control' name='comment' rows=3>" + comment + "</textarea>" +
	    "<div class=;form-group'>Rating: <input class='form-control' type='number' name='rating' min='1' max='10' step='0.5' value='" + rating + "'></div>" + 

    "<input type='hidden' name='id' value='" + id + "'>" +
    "<input type='hidden' name='place_id' value='" + id + "'>"

	var toReplace = document.getElementById('review-' + id);
	currentContent = toReplace.innerHTML;
	toReplace.innerHTML = editBox;

	document.getElementById('editDeleteButtons').style.display = "none";
	document.getElementById('saveCancelButtons').style.display = "";
}

function cancelEditReview(id, title, comment, rating)
{
	document.getElementById('review-' + id).innerHTML = currentContent;

	document.getElementById('editDeleteButtons').style.display = "";
	document.getElementById('saveCancelButtons').style.display = "none";
}