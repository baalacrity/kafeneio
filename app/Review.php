<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Review extends Model
{
	public function user()
    {
    	return $this->belongsTo(User::class);
    }

    public static function checkIfAlreadyHasReview($id)
    {
    	return DB::table('reviews')->where('user_id', '=', $id)->get();
    }

    public static function getMeanRating($id)
    {
    	if (DB::table('reviews')->where('place_id', '=', $id)->doesntExist())
    		return "None";
    	else
    		return number_format(DB::table('reviews')->where('place_id', '=', $id)->avg('rating'), 1) . "/10";
    }
}
